<?php
use Nette\Neon\Neon;
use Nette\Utils\Finder;

const WWW_DIR = __DIR__;

require WWW_DIR . "/vendor/autoload.php";

$filename = WWW_DIR . "/config.neon";
$cfg = Neon::decode(file_get_contents($filename));
$dbcfg = $cfg["database"]["default"];
try {
  $conn = new Nette\Database\Connection($dbcfg["dsn"], $dbcfg["user"], $dbcfg["password"]);
  $structure = new Nette\Database\Structure($conn, new Nette\Caching\Storages\DevNullStorage);
  $db = new Nette\Database\Context($conn, $structure);
  $rows = $db->query("SHOW DATABASES");
  foreach($rows as $row) {
    echo "Found database {$row[0]}\n";
  }
} catch(Exception $e) {
  echo "Error when connection to db: " . $e->getMessage();
  exit(1);
}

echo "\n";
echo "SQL files: \n";
$files = Finder::findFiles("*.sql")->in(WWW_DIR . "/sqls");
foreach($files as $file) {
  echo $file;
  Nette\Database\Helpers::loadFromFile($conn, $file);
  echo " - executed\n";
}

echo "\n";
echo "Checking data sanity\n";
$rows = $db->table("test1");
foreach($rows as $row) {
  if(!is_string($row->key)) {
    echo "Error: key is not string in row $row->id\n";
    exit(1);
  }
  if(!is_numeric($row->value)) {
    echo "Error: value is not number in row $row->id\n";
    exit(1);
  }
  echo "Row $row->id is fine.\n";
}
?>