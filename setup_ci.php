<?php
use Nette\Neon\Neon;

const WWW_DIR = __DIR__;

require WWW_DIR . "/vendor/autoload.php";

$filename = WWW_DIR . "/config.neon";
$cfg = Neon::decode(file_get_contents($filename));
$cfg["database"]["default"]["dsn"] = "mysql:host=mysql;dbname=test";
unlink($filename);
file_put_contents($filename, Neon::encode($cfg, Neon::BLOCK));
?>